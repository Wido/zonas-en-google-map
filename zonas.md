# ¿Cómo graficar zonas en Google Map con kotlin?

Este tutorial muestra como podemos graficar una zona, región o área clickeable en Google Map. El área se grafica utlizando poligonos y en este caso se utlizo Kotlin para ello.

## Hablita las Apis necesarias

Primero debes tener habilitada la Api de Google Map, para graficar los poligonos no es necesario otras Apis. Debes tener una API Key y SH-1 fingerprint, para ello visita [este sitio](https://developers.google.com/maps/documentation/android-sdk/get-api-key).

## Poligonnos

Los poligonos son formas geometricas similares a las polilineas. A diferencia de las polilineas los poligonos son cerrados. Para crearlos necesitamos especificar un conjunto de coordenadas, el mismo objeto se encarga de cerrar automaticamente la forma en el mapa.

1. Crear el objeto PolygonOptions y agregar los puntos. Cada punto es un objeto LatLng. 
2. Para hacer clickeables a los poligonos debes cambiar la propiedad Poylgon.isclickeable = true
3. Para agregar el poligono al mapa debes llamar a mMap.addPolygon(), con PolygonOptions como parametro
4. Para guardar información a cerca del poligono  se puede utlizar polygon.tag = 'poligono de ejemplo'
5. Para establecer un color de fondo y transparente, podemos usar: polygon.fillColor = Color.argb(70,0,0,150)

```
val polygon = mMap.addPolygon(PolygonOptions()
            .add(LatLng(-24.7189713,-65.4246511))
            .add(LatLng(-24.72114,-65.391592))
            .add(LatLng(-24.727309, -65.390809))
            .add(LatLng(-24.736229, -65.421791))
        )
        polygon.tag = "Poligono4"
        polygon.fillColor = Color.argb(70,0,100,100)
        polygon.strokeWidth = 2.5f
        polygon.isClickable = true`
```

## Capturando el poligono clickeado

```
mMap.setOnPolygonClickListener(object : GoogleMap.OnPolygonClickListener {
            override fun onPolygonClick(polygon: Polygon) {
                Toast.makeText(this,"Haz presionado el poligono: "+polygon.tag.tostring(), Toast.LENGTH_SHORT).show()
            }
        })
```
