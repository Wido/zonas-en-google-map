class MapsZoneActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private lateinit var bottomSheetBehavior: BottomSheetBehavior<View>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps_zone)
        
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        //Bottom Sheet para mostrar información de acuerdo a la zona seleccionada
        bottomSheetBehavior = BottomSheetBehavior.from(bottom_sheet_layout)

    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        setUpMap()

        // definición del poligono 3
        val polygon3 = mMap.addPolygon(PolygonOptions()
            .add(LatLng(-24.7175107,-65.4279127))
            .add(LatLng(-24.711895,-65.3924227))
            .add(LatLng(-24.72114,-65.391592))
            .add(LatLng(-24.7189713,-65.4246511))
        )
      
        polygon3.tag = "Poligono 3"
        //establecemos un color transparente de fondo de la zona
        polygon3.fillColor = Color.argb(70,0,0,150)
        polygon3.strokeWidth = 2.5f
        polygon3.isClickable = true

        // definición del poligono 4
        val polygon4 = mMap.addPolygon(PolygonOptions()
            .add(LatLng(-24.7189713,-65.4246511))
            .add(LatLng(-24.72114,-65.391592))
            .add(LatLng(-24.727309, -65.390809))
            .add(LatLng(-24.736229, -65.421791))
        )
        polygon4.tag = "Poligono 4"
        polygon4.fillColor = Color.argb(70,0,100,100)
        polygon4.strokeWidth = 2.5f
        polygon4.isClickable = true

        //captura la zona seleccionada
        mMap.setOnPolygonClickListener(object : GoogleMap.OnPolygonClickListener {
            override fun onPolygonClick(polygon: Polygon) {
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
                tv_zone_id.text = polygon.tag.toString()
            }
        })
        
    }

}
